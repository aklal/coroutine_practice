import kotlinx.coroutines.*


fun main() = runBlocking {
    val scope = CustomScope()

    val job = scope.launch {
        println("Launching in custom scope")
        val value = AggregateUserDataUseCase(resolveCurrentUserImpl2, fetchUserCommentsImpl2, fetchSuggestedFriendsImpl2).aggregateDataForCurrentUser()
        println("agg: ${value}")
    }
    job.join()

    scope.onStop() //cancels all the coroutines
}
