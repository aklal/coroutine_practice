import kotlinx.coroutines.*
import java.io.Closeable

class AggregateUserDataUseCase(
    private val resolveCurrentUser: suspend () -> UserEntity,
    private val fetchUserComments: suspend (UserId) -> List<CommentEntity>,
    private val fetchSuggestedFriends: suspend (UserId) -> List<FriendEntity>,
    dispatchers: DispatcherProvider = DefaultDispatcherProvider()
) : Closeable {
    private val coroutineErrorHandler = CoroutineExceptionHandler { context, error ->
        println("Problems with Coroutine: $error")
    }
    private val ctxt = dispatchers.default() + coroutineErrorHandler
    private val applicationScope = CoroutineScope(SupervisorJob() + ctxt)

    private val timeout = 2000L

    private suspend fun <T> fetchInformationAsync(userId: String, fetch: suspend (id: String) -> List<T>): Deferred<List<T>>{
        return withTimeout(timeout) {
            applicationScope.async(context = ctxt) {
                fetch(userId)
            }
        }
    }

    suspend fun aggregateDataForCurrentUser(): AggregatedData {
        val userRequest = applicationScope.async(context = ctxt) {
            resolveCurrentUser()
        }
        val user = userRequest.await()

        val comment = fetchInformationAsync<CommentEntity>(user.id, fetchUserComments)
        val suggestedFriends = fetchInformationAsync<FriendEntity>(user.id, fetchSuggestedFriends)

        return AggregatedData(user, comment.await(), suggestedFriends.await())
    }

    override fun close() {
        applicationScope.cancel()
    }
}

/**
 *
 * The following is already available on classpath.
 * Please do not uncomment this code or modify.
 * This is only for your convenience to copy-paste code into the IDE


package coroutines

data class AggregatedData(
val user: UserEntity,
val comments: List<CommentEntity>,
val suggestedFriends: List<FriendEntity>
)

typealias UserId = String

data class UserEntity(val id: UserId, val name: String)

data class CommentEntity(val id: String, val content: String)

data class FriendEntity(val id: String, val name: String)
 **/
