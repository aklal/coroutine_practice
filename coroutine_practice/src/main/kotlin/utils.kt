import kotlin.random.Random

data class AggregatedData(
    val user: UserEntity,
    val comments: List<CommentEntity>,
    val suggestedFriends: List<FriendEntity>
)

typealias UserId = String

data class UserEntity(val id: UserId, val name: String)

data class CommentEntity(val id: String, val content: String)

data class FriendEntity(val id: String, val name: String)


val listUserEntity = listOf<UserEntity>(
    UserEntity(id = "12", name = "titi"),
    UserEntity(id = "1", name = "bubu"),
    UserEntity(id = "2", name = "zozo")
)

val mapUserWithComments = mapOf<UserId, List<CommentEntity>>(
    "12" to listOf<CommentEntity>(
        CommentEntity("1", "comment titi 1"),
        CommentEntity("2", "comment titi 2"),
        CommentEntity("3", "comment titi 3"),
        CommentEntity("4", "comment titi 4")
    ),
    "1" to listOf<CommentEntity>(
        CommentEntity("5", "comment bubu 1"),
        CommentEntity("6", "comment bubu 2"),
        CommentEntity("7", "comment bubu 3")
    ),
    "2" to listOf<CommentEntity>(
        CommentEntity("8", "comment zozo 1"),
        CommentEntity("9", "comment zozo 2")
    ),
)

val mapUserFriends = mapOf<UserId, List<FriendEntity>>(
    "12" to listOf<FriendEntity>(
        FriendEntity("1", "titi"),
        FriendEntity("2", "toto")
    ),
    "1" to listOf<FriendEntity>(
        FriendEntity("5", "baba 1")
    ),
    "2" to listOf<FriendEntity>(
        FriendEntity("8", "zeze 1"),
        FriendEntity("9", "zuzu 2")
    ),
)


suspend fun resolveCurrentUserImpl(): UserEntity {
    val randomUserRank = Random.nextInt(0, listUserEntity.size)
    return listUserEntity[randomUserRank]
}

suspend fun fetchUserCommentsImpl(id: UserId): List<CommentEntity>? {
    return mapUserWithComments[id]
}

suspend fun fetchSuggestedFriends(id: UserId): List<FriendEntity>? {
    return mapUserFriends[id]
}


val resolveCurrentUserImpl2: suspend () -> UserEntity = {
    val randomUserRank = Random.nextInt(0, listUserEntity.size)
    listUserEntity[randomUserRank]
}

val fetchUserCommentsImpl2: suspend (UserId) -> List<CommentEntity> = { id ->
    mapUserWithComments[id]?.let { it }?:throw NullPointerException()
}


val fetchSuggestedFriendsImpl2: suspend (UserId) -> List<FriendEntity> = { id ->
    mapUserFriends[id]?.let { it }?:throw NullPointerException()
}

