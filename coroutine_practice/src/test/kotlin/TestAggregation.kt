import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.junit.JUnitAsserter.assertNotEquals

class TestAggregation {
    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesTestRule = CoroutineTestRule()

    @Test
    fun testWithRunBlocking() = runBlocking<Unit> {
        val agg = AggregateUserDataUseCase(
            resolveCurrentUserImpl2,
            fetchUserCommentsImpl2,
            fetchSuggestedFriendsImpl2
        ).aggregateDataForCurrentUser()

        assertNotNull(agg)
    }

    /* runBlockingTest raises `This job has not completed yet` if no dispather are provided
    @ExperimentalCoroutinesApi
    @Test
    fun testWithRunBlockingTest() = runBlockingTest {
        val agg = AggregateUserDataUseCase(resolveCurrentUserImpl2, fetchUserCommentsImpl2, fetchSuggestedFriendsImpl2).aggregateDataForCurrentUser()
        assertNotNull(agg)
    }*/

    @ExperimentalCoroutinesApi
    @Test
    fun testWithRunDispatcherBlockingTest() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val agg = AggregateUserDataUseCase(
            resolveCurrentUserImpl2,
            fetchUserCommentsImpl2,
            fetchSuggestedFriendsImpl2,
            coroutinesTestRule.testDispatcherProvider
        ).aggregateDataForCurrentUser()

        assertNotNull(agg)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testAggregation() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val resolveCurrentUserImplForTest: suspend () -> UserEntity = {
            listUserEntity[0]
        }

        val agg = AggregateUserDataUseCase(
            resolveCurrentUserImplForTest,
            fetchUserCommentsImpl2,
            fetchSuggestedFriendsImpl2,
            coroutinesTestRule.testDispatcherProvider
        ).aggregateDataForCurrentUser()


        assertEquals(agg.user, listUserEntity[0])
        assertEquals(agg.comments, mapUserWithComments["12"])
        assertEquals(agg.suggestedFriends, mapUserFriends["12"])
    }
}